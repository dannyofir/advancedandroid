package com.example.danny.advancedandroid.home;

import com.bluelinelabs.conductor.Controller;
import com.example.danny.advancedandroid.di.ConrollerKey;
import com.example.danny.advancedandroid.trending.TrendingReposComponent;
import com.example.danny.advancedandroid.trending.TrendingReposController;
import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {
        TrendingReposComponent.class
})
public abstract class TestScreenBindingModule {

    @Binds
    @IntoMap
    @ConrollerKey(TrendingReposController.class)
    abstract AndroidInjector.Factory<? extends Controller> bindTrendingReposController (TrendingReposComponent.Builder builder);

}
