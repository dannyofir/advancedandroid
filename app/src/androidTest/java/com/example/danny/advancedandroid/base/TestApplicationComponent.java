package com.example.danny.advancedandroid.base;

import com.example.danny.advancedandroid.data.TestRepoServiceModule;
import com.example.danny.advancedandroid.networking.ServiceModule;
import com.example.danny.advancedandroid.trending.TrendingReposControllerTest;
import com.example.danny.advancedandroid.ui.NavigationModule;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        TestActivityBindingModule.class,
        TestRepoServiceModule.class,
        ServiceModule.class,
        NavigationModule.class
})
public interface TestApplicationComponent extends ApplicationComponent {
    void inject(TrendingReposControllerTest trendingReposControllerTest);
}
