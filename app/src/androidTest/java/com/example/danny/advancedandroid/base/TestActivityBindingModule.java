package com.example.danny.advancedandroid.base;

import android.app.Activity;
import com.example.danny.advancedandroid.home.MainActivity;
import com.example.danny.advancedandroid.home.TestMainActivityComponent;
import dagger.Binds;
import dagger.Module;
import dagger.android.ActivityKey;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {
        TestMainActivityComponent.class,
})
public abstract class TestActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity.class)
    abstract AndroidInjector.Factory<? extends Activity> provideMainActivityInjector(TestMainActivityComponent.Builder builder);

}
