package com.example.danny.advancedandroid.home;

import com.bluelinelabs.conductor.Controller;
import com.example.danny.advancedandroid.details.RepoDetailsComponent;
import com.example.danny.advancedandroid.details.RepoDetailsController;
import com.example.danny.advancedandroid.di.ConrollerKey;
import com.example.danny.advancedandroid.trending.TrendingReposComponent;
import com.example.danny.advancedandroid.trending.TrendingReposController;
import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjector;
import dagger.multibindings.IntoMap;

@Module(subcomponents = {
        TrendingReposComponent.class,
        RepoDetailsComponent.class,
})
public abstract class MainScreenBindingModule {

    @Binds
    @IntoMap
    @ConrollerKey(TrendingReposController.class)
    abstract AndroidInjector.Factory<? extends Controller> bindTrendingReposInjector (TrendingReposComponent.Builder builder);

    @Binds
    @IntoMap
    @ConrollerKey(RepoDetailsController.class)
    abstract AndroidInjector.Factory<? extends Controller> bindRepoDetailsInjector (RepoDetailsComponent.Builder builder);

}
