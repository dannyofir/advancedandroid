package com.example.danny.advancedandroid.di;

import com.bluelinelabs.conductor.Controller;
import dagger.MapKey;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@MapKey
@Target(ElementType.METHOD)
public @interface ConrollerKey {
    Class<? extends Controller> value();
}
