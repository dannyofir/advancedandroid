package com.example.danny.advancedandroid.home;

import com.bluelinelabs.conductor.Controller;
import com.example.danny.advancedandroid.R;
import com.example.danny.advancedandroid.base.BaseActivity;
import com.example.danny.advancedandroid.trending.TrendingReposController;

public class MainActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected Controller initialScreen() {
        return new TrendingReposController();
    }
}
