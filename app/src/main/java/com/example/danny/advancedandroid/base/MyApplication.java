package com.example.danny.advancedandroid.base;

import android.app.Application;
import com.example.danny.advancedandroid.BuildConfig;
import com.example.danny.advancedandroid.di.ActivityInjector;
import timber.log.Timber;

import javax.inject.Inject;

public class MyApplication extends Application {

    @Inject
    ActivityInjector activityInjector;

    protected ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = initComponent();
        component.inject(this);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    protected ApplicationComponent initComponent() {
        return DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    public ActivityInjector getActivityInjector() {
        return activityInjector;
    }
}
