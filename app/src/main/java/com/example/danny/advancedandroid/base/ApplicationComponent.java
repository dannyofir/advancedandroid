package com.example.danny.advancedandroid.base;

import com.example.danny.advancedandroid.data.RepoServiceModule;
import com.example.danny.advancedandroid.networking.ServiceModule;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {
        ApplicationModule.class,
        ActivityBindingModule.class,
        ServiceModule.class,
        RepoServiceModule.class,
})
public interface ApplicationComponent {
    void inject(MyApplication myApplication);
}
